﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    internal Logic bigCube;
    internal Logic mediumCube;
    internal Logic smallCube;
    private bool played;

    private void Awake()
    {
        bigCube = GameObject.Find("bigCube").GetComponent<Logic>();
        mediumCube = GameObject.Find("mediumCube").GetComponent<Logic>();
        smallCube = GameObject.Find("smallCube").GetComponent<Logic>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!played)
        {
            if (bigCube.isDone && mediumCube.isDone && smallCube.isDone)
            {
                GetComponent<AudioSource>().Play();
                played = true;
            }
        }
    }
}
